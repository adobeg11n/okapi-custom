/*===========================================================================
  Copyright (C) 2009-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xmlstream;

import java.io.File;
import java.net.URL;
import java.util.regex.Pattern;

import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.Tag;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.SubFilter;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.filters.abstractmarkup.AbstractMarkupEventBuilder;
import net.sf.okapi.filters.abstractmarkup.AbstractMarkupFilter;
import net.sf.okapi.filters.abstractmarkup.ExtractionRuleState;
import net.sf.okapi.filters.yaml.TaggedFilterConfiguration;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.Event;

@UsingParameters(Parameters.class)
public class XmlStreamFilter extends AbstractMarkupFilter {

	private Parameters parameters;
	private static final String CDATA_START_REGEX = "<\\!\\[CDATA\\[";
	private static final String CDATA_END_REGEX = "\\]\\]>";
	private static final Pattern CDATA_START_PATTERN = Pattern.compile(CDATA_START_REGEX);
	private static final Pattern CDATA_END_PATTERN = Pattern.compile(CDATA_END_REGEX);
	private int cdataSectionIndex;
	private IFilter cdataFilter;
	private ExtractionRuleState ruleState; 
	/* MATCH PATTERN
		<.*\s*localizable.*\s*id=\s*[\"\'](.*)[\"\'].*\s*>
	*/ 
	private static Pattern localizableIdRe = Pattern.compile("<.*\\s*localizable.*\\s*id=\\s*[\\\"\\'].*\\.html[\\\"\\'].*\\s*>");
	public XmlStreamFilter() {
		super();			
		setMimeType(MimeTypeMapper.XML_MIME_TYPE);
		setFilterWriter(createFilterWriter());
		setParameters(new Parameters());
		setName("okf_xmlstream"); //$NON-NLS-1$
		setDisplayName("XML Stream Filter"); //$NON-NLS-1$
		addConfiguration(new FilterConfiguration(getName(), MimeTypeMapper.XML_MIME_TYPE,
				getClass().getName(), "XML Stream", "Large XML Documents", //$NON-NLS-1$
				Parameters.DEFAULT_PARAMETERS));
		addConfiguration(new FilterConfiguration(getName()+"-dita", MimeTypeMapper.XML_MIME_TYPE,
				getClass().getName(), "DITA", "DITA XML", //$NON-NLS-1$
				Parameters.DITA_PARAMETERS, ".dita;"));
		addConfiguration(new FilterConfiguration(getName()+"-JavaPropertiesHTML", MimeTypeMapper.XML_MIME_TYPE,
				getClass().getName(), "Java Properties XML + HTML", "Java Properties XML with Embedded HTML", //$NON-NLS-1$
				Parameters.PROPERTY_XML_PARAMETERS));
	}

	/**
	 * Initialize rule state and parser. Called before processing of each input.
	 */
	@Override
	protected void startFilter() {
		super.startFilter();
//		getEventBuilder().initializeCodeFinder(getConfig().isUseCodeFinder(), 
//				getConfig().getCodeFinderRules());
		getEventBuilder().initializeCodeFinder(getConfig().isUseCodeFinder(), 
				getConfig().getCodeFinderRules(), getConfig().isUseBalancedParanthesisCodes());
		cdataFilter = getCdataFilter();
		ruleState= getRuleState();
	}

	@Override
	protected void handleStartTag(StartTag startTag) {
		super.handleStartTag(startTag);
	}

	@Override
	protected void handleEndTag(EndTag endTag) {
		super.handleEndTag(endTag);
	}
	
	/*
	 * (non-Javadoc)
	 * @see net.sf.okapi.common.markupfilter.BaseMarkupFilter#normalizeName(java. lang.String)
	 */
	@Override
	protected String normalizeAttributeName(String attrName, String attrValue, Tag tag) {
		// normalize values for XML
		String normalizedName = attrName;

		// <x xml:lang="en">
		if (attrName.equalsIgnoreCase("xml:lang")) {
			normalizedName = Property.LANGUAGE;
		}

		return normalizedName;
	}

	@Override
	protected TaggedFilterConfiguration getConfig() {
		return parameters.getTaggedConfig();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.okapi.common.filters.IFilter#setParameters(net.sf.okapi.common .IParameters)
	 */
	public void setParameters(IParameters params) {
		this.parameters = (Parameters) params;
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.okapi.common.filters.IFilter#getParameters()
	 */
	public IParameters getParameters() {
		return parameters;
	}

	/**
	 * Initialize filter parameters from a URL.
	 * 
	 * @param config
	 */
	public void setParametersFromURL(URL config) {
		parameters = new Parameters(config);
	}

	/**
	 * Initialize filter parameters from a Java File.
	 * 
	 * @param config
	 */
	public void setParametersFromFile(File config) {
		parameters = new Parameters(config);
	}

	/**
	 * Initialize filter parameters from a String.
	 * 
	 * @param config
	 */
	public void setParametersFromString(String config) {
		parameters = new Parameters(config);
	}
	
	/**
	 * @return the {@link AbstractMarkupEventBuilder}
	 */
	public AbstractMarkupEventBuilder getEventBuilder() {
		return (AbstractMarkupEventBuilder)super.getEventBuilder();
	}
	
	/**
	 * Handle CDATA sections.
	 * 
	 * @param tag
	 */
	protected void handleCdataSection(Tag tag) {
		// end any skeleton so we can start CDATA section with subfilter
		
		EventBuilder eventBuilder= getEventBuilder();
		
		boolean isHtmlText = false;
		
		
		
		if (eventBuilder.hasUnfinishedSkeleton()) {
			endDocumentPart();
		}
		String cdataWithoutMarkers = CDATA_START_PATTERN.matcher(tag.toString()).replaceFirst("");
		cdataWithoutMarkers = CDATA_END_PATTERN.matcher(cdataWithoutMarkers).replaceFirst("");
		if((cdataFilter.getMimeType().equals(MimeTypeMapper.HTML_MIME_TYPE) ))
			{
				Event e = eventBuilder.getPreviousFilterEvent();
				if(e!=null)
				{
					DocumentPart dp = e.getDocumentPart();
					if (dp != null)
					{
						String s = dp.toString();						  
						if(s != null)
						{
							//check if the tag is 'localizable' and it's id attribute ends in .html
							isHtmlText = localizableIdRe.matcher(s).matches(); 
						}
					}
				 
				}
			}
		if ( ruleState.isExludedState() ) {
			// Excluded content
			addToDocumentPart(tag.toString());
		} else { // Content to extract
			startTextUnit(new GenericSkeleton("<![CDATA["));
			
			if (cdataFilter != null  ) {
				
				if(!(cdataFilter.getMimeType().equals( MimeTypeMapper.HTML_MIME_TYPE)) ||
						(cdataFilter.getMimeType().equals( MimeTypeMapper.HTML_MIME_TYPE) && isHtmlText))
				{
				String parentId = eventBuilder.findMostRecentParentId();
				if (parentId == null) parentId = getDocumentId().getLastId();
				
				String parentName = eventBuilder.findMostRecentParentName();
				if (parentName == null) parentName = getDocumentId().getLastId();
				
				SubFilter cdataSubfilter = new SubFilter(cdataFilter, 
						null, // we don't encode cdata
						++cdataSectionIndex, parentId, parentName);
				eventBuilder.addFilterEvents(cdataSubfilter.getEvents(new RawDocument(cdataWithoutMarkers, getSrcLoc())));
				addToTextUnit(cdataSubfilter.createRefCode());
				}
				else
				{
					// we assume the CDATA is plain text take it as is				
					addToTextUnit(cdataWithoutMarkers);				
				}
			} else {
				// we assume the CDATA is plain text take it as is				
				addToTextUnit(cdataWithoutMarkers);				
			}
			setTextUnitType(ITextUnit.TYPE_CDATA);
			setTextUnitMimeType(MimeTypeMapper.PLAIN_TEXT_MIME_TYPE);			
			endTextUnit(new GenericSkeleton("]]>"), false);
		}
	}
}
