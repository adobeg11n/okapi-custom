/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/


package net.sf.okapi.filters.xmlstream;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class XmlStreamSubfilterTest {
	
	private static LocaleId locEN = LocaleId.fromString("en");
	private XmlStreamFilter filter;
	
	@Before
	public void setUp() throws Exception {
		filter = new XmlStreamFilter();
	}
	
	@Test
	public void testSimple() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.yml");
		URL inputUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Translate me.", tu.getSource().toString());
		
		// Make sure only one TU was produced
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNull(tu);
	}
	
	@Test
	public void testNestedTextunits() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.yml");
		List<Event> events;
		String xml = "<xml><x1>foo</x1>bar</xml>";
		RawDocument rd = new RawDocument(xml, locEN);
		events = getEvents(filter, rd, configUrl);
//		assertEquals("", FilterTestDriver.getTextUnit(events, 1).getSource().toString());
		assertEquals("foo", FilterTestDriver.getTextUnit(events, 1).getSource().toString());
		assertEquals("bar", FilterTestDriver.getTextUnit(events, 2).getSource().toString());
	}
	
	@Test
	public void testTranslateAttributeSubfilter() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/translate-attr-subfilter.yml");
		URL inputUrl = XmlStreamConfigurationTest.class
				.getResource("/translate-attr-subfilter.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Translate me.", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<a>Translate me 4.<a>", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNull(tu);
	}
	
	private ArrayList<Event> getEvents(XmlStreamFilter filter, RawDocument doc, URL params) {           
        ArrayList<Event> list = new ArrayList<Event>();                         
        filter.setParametersFromURL(params);
        FilterConfigurationMapper mapper = new FilterConfigurationMapper();
        mapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
        filter.setFilterConfigurationMapper(mapper);
        filter.open(doc);
        while (filter.hasNext()) {
            Event event = filter.next();                                        
            list.add(event);                                                    
        }                                                                       
        filter.close();                                                         
        return list;                                                            
    }   
}
