package net.sf.okapi.filters.html;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import org.junit.After;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HtmlSnippetsExtTest {

	private HtmlFilter htmlFilter;
	private URL parameters;
	private LocaleId locEN = LocaleId.fromString("en");
	private LocaleId locFR = LocaleId.fromString("fr");
	private GenericContent fmt = new GenericContent();

	@Before
	public void setUp() {
		htmlFilter = new HtmlFilter();
		parameters = HtmlSnippetsExtTest.class.getResource("embeddableAttrConfiguration.yml");
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testCodesStorage () {
		String snippet = "<p>before <a href=\"there\" title=\"titleval\"/> after.</p>";
		ArrayList<Event> events = getEvents(snippet);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		List<Code> codes = tu.getSource().getFirstContent().getCodes();
		assertEquals(3, codes.size());
		assertEquals("<a href=\"", codes.get(0).getData());
		assertEquals("\" title=\"", codes.get(1).getData());
		assertEquals("\"/>", codes.get(2).getData());
		assertEquals(snippet, generateOutput(getEvents(snippet), snippet, locEN));
	}

	@Test
	public void testCombinationAttributes() {
		String snippet = "<p>before <a href=\"there\" title=\"titleval\" accesskey=\"123\"/> after.</p>";
		ArrayList<Event> events = getEvents(snippet);
		assertEquals(6, events.size());
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertEquals("there", tu.toString());
		assertEquals("titleval", FilterTestDriver.getTextUnit(events, 2).toString());
		assertEquals("123", FilterTestDriver.getTextUnit(events, 3).toString());
		tu = FilterTestDriver.getTextUnit(events, 4);
		List<Code> codes = tu.getSource().getFirstContent().getCodes();
		assertEquals(1, codes.size());
		assertEquals(snippet, generateOutput(getEvents(snippet), snippet, locEN));
	}

	private ArrayList<Event> getEvents(String snippet) {
		ArrayList<Event> list = new ArrayList<Event>();
		htmlFilter.setParametersFromURL(parameters);
		htmlFilter.open(new RawDocument(snippet, locEN));
		while (htmlFilter.hasNext()) {
			Event event = htmlFilter.next();
			list.add(event);
		}
		htmlFilter.close();
		return list;
	}

	private String generateOutput(ArrayList<Event> list, String original, LocaleId trgLang) {
		GenericSkeletonWriter writer = new GenericSkeletonWriter();
		StringBuilder tmp = new StringBuilder();
		for (Event event : list) {
			switch (event.getEventType()) {
			case START_DOCUMENT:
				writer.processStartDocument(trgLang, "utf-8", null, htmlFilter.getEncoderManager(), (StartDocument) event
						.getResource());
				break;
			case TEXT_UNIT:
				ITextUnit tu = (ITextUnit) event.getResource();
				tmp.append(writer.processTextUnit(tu));
				break;
			case DOCUMENT_PART:
				DocumentPart dp = (DocumentPart) event.getResource();
				tmp.append(writer.processDocumentPart(dp));
				break;
			case START_GROUP:
			case START_SUBFILTER:
				StartGroup startGroup = (StartGroup) event.getResource();
				tmp.append(writer.processStartGroup(startGroup));
				break;
			case END_GROUP:
			case END_SUBFILTER:
				Ending ending = (Ending) event.getResource();
				tmp.append(writer.processEndGroup(ending));
				break;
			}
		}
		writer.close();
		return tmp.toString();
	}

}
