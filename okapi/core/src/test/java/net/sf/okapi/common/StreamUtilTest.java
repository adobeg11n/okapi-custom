package net.sf.okapi.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StreamUtilTest {

	@Test
	public void testCRC() {
		//assertEquals(1018854380L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/ParamTest01.txt")));
		assertEquals(3995389683L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/safeouttest1.txt")));
		assertEquals(369693688L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/test_path1.txt")));
		//assertEquals(681066369L, StreamUtil.calcCRC(this.getClass().getResourceAsStream("/test.html")));
	}
}
