/*===========================================================================
  Copyright (C) 2008-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.wordcount.common;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.lib.extra.steps.AbstractPipelineStep;

/**
 * Base abstract class for different counter steps (word count step, character count step, etc.).
 * 
 * @version 0.1 08.07.2009
 */

public abstract class BaseCountStep extends AbstractPipelineStep {

//	protected enum CountContext {
//		CC_SOURCE,
//		CC_TARGET
//	}
	private Parameters params;
	private IdGenerator gen = new IdGenerator("ending");
	private TextContainer source;
	private StringBuilder sb;
	
	private long batchCount;
	private long batchItemCount;
	private long documentCount;
	private long subDocumentCount;
	private long groupCount;	
	
	private Hashtable<LocaleId, Long> batchCount_map = new Hashtable<LocaleId, Long>();
	private Hashtable<LocaleId, Long> batchItemCount_map = new Hashtable<LocaleId, Long>() ;
	private Hashtable<LocaleId, Long> documentCount_map = new Hashtable<LocaleId, Long>() ;
	private Hashtable<LocaleId, Long> subDocumentCount_map = new Hashtable<LocaleId, Long>() ;
	private Hashtable<LocaleId, Long> groupCount_map = new Hashtable<LocaleId, Long>() ;
	
	
	public BaseCountStep() {
		super();
		params = new Parameters();
		setParameters(params);
		setName(getName());
		setDescription(getName());
	}
	
	@Override
	protected void component_init() {
		params = getParameters(Parameters.class);
		
		// Reset counters
		batchCount = 0;
		batchItemCount = 0;
		documentCount = 0;
		subDocumentCount = 0;
		groupCount = 0;
	}

	//-------------------------
	abstract public String getName();
	abstract public String getDescription();
	abstract public String getMetric();
	abstract protected long count(TextContainer textContainer, LocaleId locale);
	abstract protected long count(Segment segment, LocaleId locale);
	abstract protected Map<LocaleId, Long> countInTextUnit(ITextUnit textUnit);
	abstract protected boolean countOnlyTranslatable();
//	abstract protected CountContext getCountContext();

	protected void saveCount(Metrics metrics, long count) {
		if (metrics == null) return;
		
		metrics.setMetric(getMetric(), count);
	}

	protected void saveCount(MultiMetrics metrics, Map<LocaleId, Long> count_map) {
		if (metrics == null) return;
		
		metrics.setMetric(getMetric(), count_map); 
	}

	public long getBatchCount() {
		return batchCount;
	}

	public long getBatchItemCount() {
		return batchItemCount;
	}

	public long getDocumentCount() {
		return documentCount;
	}

	public long getSubDocumentCount() {
		return subDocumentCount;
	}

	public long getGroupCount() {
		return groupCount;
	}

	protected void saveToMetrics(Event event, long count) {
		if (event == null) return;
		if (count == 0) return;
		
		IResource res = event.getResource();
		if (res == null) {
			res = createResource(event);
		}
		if (res == null) return;
		
		MetricsAnnotation ma = res.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			res.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
					
		saveCount(m, count);
	}
	
	protected void saveToMetrics(Event event, Map<LocaleId, Long> count_map) {
		if (event == null) return;
		if (count_map.isEmpty()) return;
		
		IResource res = event.getResource();
		if (res == null) {
			res = createResource(event);
		}
		if (res == null) return;
		
		MultiMetricsAnnotation ma = res.getAnnotation(MultiMetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MultiMetricsAnnotation();
			res.setAnnotation(ma);
		}
		
		MultiMetrics m = ma.getMetrics();		
		if (m == null) return;
					
		saveCount(m, count_map);
	}
	
	
	protected void removeFromMetrics(IResource res, String metricName) {
		MetricsAnnotation ma = res.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			res.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
		m.unregisterMetric(metricName);
	}
	
	protected void removeFromMetrics(TextContainer textContainer, String metricName) {
		if (textContainer == null) return;
		
		MetricsAnnotation ma = textContainer.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			textContainer.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
					
		m.unregisterMetric(metricName);
	}
	
	protected void removeFromMetrics(Segment seg, String metricName) {		
		if (seg == null) return;
		
		MetricsAnnotation ma = seg.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			seg.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
					
		m.unregisterMetric(metricName);
	}
	
	private IResource createResource(Event event) {
		if (event == null) return null;
		
		IResource res = event.getResource();
		if (res != null) return res;
		
		switch (event.getEventType()) {
		case END_BATCH:
		case END_BATCH_ITEM:
		case END_DOCUMENT:
		case END_SUBDOCUMENT:
		case END_GROUP:
			res = new Ending(gen.createId());
			event.setResource(res);
			break;			
		}
		
		return res;
	}

	protected void saveToMetrics(TextContainer textContainer, long count) {
		if (textContainer == null) return;
		//if (count == 0) return;
				
		MetricsAnnotation ma = textContainer.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			textContainer.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
					
		saveCount(m, count);
	}
	
	protected void saveToMetrics(Segment seg, long count) {		
		if (seg == null) return;
		if (count == 0) return;
		
		MetricsAnnotation ma = seg.getAnnotation(MetricsAnnotation.class);
		
		if (ma == null) {			
			ma = new MetricsAnnotation();
			seg.setAnnotation(ma);
		}
		
		Metrics m = ma.getMetrics();		
		if (m == null) return;
					
		saveCount(m, count);
	}
	
	//-------------------------	
	@Override
	protected Event handleStartBatch(Event event) {
		
		if(batchCount_map != null)
		batchCount_map.clear();
		return event;
	}
	
	
	
	@Override
	protected Event handleEndBatch(Event event) {
		flushBuffer();
		
		if (!params.getCountInBatch()) return event;
		if (batchCount_map.isEmpty()) return event;
		
		saveToMetrics(event, batchCount_map);
		return event;
	}
	

	//-------------------------	
	@Override
	protected Event handleStartBatchItem(Event event) {
		
		if(batchItemCount_map != null)
		batchItemCount_map.clear();
		return event;
	}
	

	@Override
	protected Event handleEndBatchItem(Event event) {
		flushBuffer();
		
		if (!params.getCountInBatchItems()) return event;
		if (batchItemCount_map.isEmpty()) return event;
		
		saveToMetrics(event, batchItemCount_map);
		return event;
	}

	//-------------------------	
	@Override
	protected Event handleStartDocument(Event event) {
		
		if(documentCount_map != null)
		documentCount_map.clear();
		return super.handleStartDocument(event); // Sets language						
	}
	
	@Override
	protected Event handleEndDocument(Event event) {
		if (!params.getCountInDocuments()) return event;
		if (documentCount_map.isEmpty()) return event;
		
		saveToMetrics(event, documentCount_map);
		return event;
	}

	//-------------------------
	@Override
	protected Event handleStartSubDocument(Event event) {
		
		if(subDocumentCount_map != null)
		subDocumentCount_map.clear();
		return event;
	}
		
	@Override
	protected Event handleEndSubDocument(Event event) {
		if (!params.getCountInSubDocuments()) return event;
		if (subDocumentCount_map.isEmpty()) return event;
		
		saveToMetrics(event, subDocumentCount_map);
		return event;
	}
	
	//-------------------------
	@Override
	protected Event handleStartGroup(Event event) {
		
		if(groupCount_map != null)
		groupCount_map.clear();
		return event;
	}
	
	
	@Override
	protected Event handleEndGroup(Event event) {		
		if (!params.getCountInGroups()) return event;
		if (groupCount_map.isEmpty()) return event;
		
		saveToMetrics(event, groupCount_map);
		return event;
	}

	//-------------------------
	
//	private long countInContainer(TextContainer tc) {
//		if (tc == null) return 0;
//		
//		// Individual segments metrics
//		long segmentCount;
//		long textContainerCount;
//		ISegments segs = tc.getSegments();
//		if (segs != null) {
//			for (Segment seg : segs) {
//				segmentCount = count(seg);
//				saveToMetrics(seg, segmentCount);
//			}
//		}
//		// TC metrics
//		textContainerCount = count(tc);
//		saveToMetrics(tc, textContainerCount);
//		return textContainerCount; 
//	}
	
//	private long countInSource(TextUnit tu) {
//		if (tu == null) return 0;
//		
//		// Individual segments metrics
//		long segmentCount;
//		long textContainerCount;
//		ISegments segs = tc.getSegments();
//		if (segs != null) {
//			for (Segment seg : segs) {
//				segmentCount = count(seg);
//				saveToMetrics(seg, segmentCount);
//			}
//		}
//		// TC metrics
//		textContainerCount = count(tc);
//		saveToMetrics(tc, textContainerCount);
//		return textContainerCount; 
//	}
//	
//	private long countInTarget(TextUnit tu, LocaleId targetLocale) {
//		if (tu == null) return 0;
//		
//		// Individual segments metrics
//		long segmentCount;
//		long textContainerCount;
//		ISegments segs = tc.getSegments();
//		if (segs != null) {
//			for (Segment seg : segs) {
//				segmentCount = count(seg);
//				saveToMetrics(seg, segmentCount);
//			}
//		}
//		// TC metrics
//		textContainerCount = count(tc);
//		saveToMetrics(tc, textContainerCount);
//		return textContainerCount; 
//	}
	
	
	protected TextContainer getSource() {
		return source;
	}
	
//	protected LocaleId getLocale() {
//		switch (getCountContext()) {
//		default:
//			return getSourceLocale();
//			
//		case CC_TARGET:
//			return getTargetLocale();
//		}
//	}
	
	private void flushBuffer() {
		if (params.getBufferSize() == 0) return; 
		if (sb == null) return;
		
//		System.out.println("====== flush");
//		System.out.println(sb.toString());
//		System.out.println("======");
		ITextUnit tu = new TextUnit("temp", sb.toString());
		sb = null;
		updateCounts(tu, null);
	}
	
	private void updateCounts(ITextUnit tu, Event event) {
		Map<LocaleId,Long> textUnitCount_map = countInTextUnit(tu);
		Long textUnitCount =0L,total_textUnitCount=0L;		
				
		// Whole TU metrics		
		if(!textUnitCount_map.isEmpty()){
			
			//Saves value corresponding to each locale
			for (LocaleId locale : textUnitCount_map.keySet()) {
				
				textUnitCount = textUnitCount_map.get(locale);
				total_textUnitCount = total_textUnitCount + textUnitCount;
				
					
				saveToMetrics(event, total_textUnitCount); // Saves in annotations of the whole TU
					if (params.getCountInBatch())
					{
						if(batchCount_map.containsKey(locale))
							batchCount_map.put(locale, batchCount_map.get(locale) + textUnitCount);
						else
							batchCount_map.put(locale, textUnitCount);
					}
					if (params.getCountInBatchItems())
					{
						if(batchItemCount_map.containsKey(locale))
							batchItemCount_map.put(locale, batchItemCount_map.get(locale) + textUnitCount);
						else
							batchItemCount_map.put(locale, textUnitCount);
					}
					if (params.getCountInDocuments())
					{
						if(documentCount_map.containsKey(locale))
							documentCount_map.put(locale, documentCount_map.get(locale) + textUnitCount);
						else
							documentCount_map.put(locale, textUnitCount);
					}
					if (params.getCountInSubDocuments())
					{
						if(subDocumentCount_map.containsKey(locale))
							subDocumentCount_map.put(locale, subDocumentCount_map.get(locale) + textUnitCount);
						else
							subDocumentCount_map.put(locale, textUnitCount);
					}
					if (params.getCountInGroups())
					{
						if(groupCount_map.containsKey(locale))
							groupCount_map.put(locale, groupCount_map.get(locale) + textUnitCount);
						else
							groupCount_map.put(locale, textUnitCount);
					}
				}
			
					
			}
	}
	
	@Override
	protected Event handleTextUnit(Event event) {		
		ITextUnit tu = event.getTextUnit();
		
		if (tu.isEmpty()) return event;
		if (!tu.isTranslatable() && countOnlyTranslatable()) return event;
		
		source = tu.getSource();
		
		if (params.getBufferSize() > 0) {
			if (sb == null) {
				sb = new StringBuilder(params.getBufferSize());
			}
			// Non-translatable text doesn't get here
			String srcText = tu.getSource().getUnSegmentedContentCopy().getText();
			sb.append(srcText);
			//System.out.println(srcText);
			if (sb.length() >= params.getBufferSize()) {
				flushBuffer();
			}
			return event;
		}
		
		updateCounts(tu, event);
		return event;
	}		
}
