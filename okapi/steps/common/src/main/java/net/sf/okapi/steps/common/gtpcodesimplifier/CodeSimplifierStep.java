/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.common.gtpcodesimplifier;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.CodeSimplifier;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.SkeletonUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * !!! It's important to include this step in a pipeline before any source-copying or leveraging steps, because it can modify 
 * codes in the source, and target codes will easily get desynchronized with their sources.
 * The best place for this step -- right after the filter.  
 * 
 * Modified for GTP - only simplifies adjacent opening place holder tags and adjacent closing place holder tags.
 * Merging of stand alone placeholder tags has ben disabled for APO.
 */
@UsingParameters(Parameters.class)
public class CodeSimplifierStep extends BasePipelineStep {
	private boolean toProcess;
	private Parameters params;

	public CodeSimplifierStep() {
		super();
		toProcess=false;
		params = new Parameters();
	}
	
	public Event handleStartDocument(Event event)
	{
		String fname=event.getStartDocument().getName();
		String extn=Util.getExtension(fname).substring(1);
		if(extn.equals(params.getFtname()))
			toProcess=true;
		else toProcess=false;
		return super.handleStartDocument(event);
		
	}
	
	public Event handleEndDocument(Event event)
	{
		toProcess=false;
		return super.handleEndDocument(event);
	}
	@Override
	public String getDescription() {
		return "Merges adjacent opening placeholder and adjacent closing placeholder inline codes " 
			+ " in the source part of a text unit."
			+ " Expects: filter events. Sends back: filter events.";
	}

	@Override
	public String getName() {
		return "GTP Inline Codes Simplifier";
	}
	
	@Override
	public IParameters getParameters() {
		return params;
	}
	
	@Override
	public void setParameters(IParameters params) {
		this.params = (Parameters) params;
	}
	
	@Override
	protected Event handleTextUnit(Event event) {
		ITextUnit tu = event.getTextUnit();
		if(toProcess==true)
		{
			simplifyCodes(tu);
		}
		//TextUnitUtil.convertTextParts(tu.getSource());
		return super.handleTextUnit(event);
	}
	
	private void simplifyCodes (ITextUnit textUnit) {
		Logger localLogger = LoggerFactory.getLogger(CodeSimplifierStep.class);
		if (textUnit == null) {
			localLogger.warn("Text unit is null.");
			return;
		}
		
		if (textUnit.getTargetLocales().size() > 0) {
			localLogger.warn(String.format("Text unit %s has one or more targets, " +
					"desinchronization of codes in source and targets is possible.", textUnit.getId()));
		}
		
		TextContainer tc = textUnit.getSource();
		
		if (textUnit.getSource().hasBeenSegmented()) {
			localLogger.error("Can't process segmented text.");
			throw new UnsupportedOperationException("Can't process segmented text.");
		}
		else {
			TextFragment tf = tc.getUnSegmentedContentCopy();  			
			simplifyCodes(tf);
			textUnit.setSourceContent(tf); // Because we modified a copy			
		}
	}
	
	private void simplifyCodes (TextFragment tf) {
		CodeSimplifier simplifier = new CodeSimplifier();
		simplifier.simplifyOpeningClosing(tf);
		simplifier.simplifyEmptyOpeningClosing(tf);
	}
}
