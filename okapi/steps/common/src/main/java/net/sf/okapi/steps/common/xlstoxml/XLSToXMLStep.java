package net.sf.okapi.steps.common.xlstoxml;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.commons.io.FileUtils;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.steps.common.codesimplifier.Parameters;

@UsingParameters()
public class XLSToXMLStep extends BasePipelineStep {

	// private Parameters params;
	private URI outputURI;
	@StepParameterMapping(parameterType = StepParameterType.OUTPUT_URI)
	public void setOutputURI (URI outputURI) {
		this.outputURI = outputURI;
	}
	public XLSToXMLStep() {
		super();
		// params = new Parameters();
	}

	@Override
	public String getName() {

		return "XLS to XML Conversion";
	}

	@Override
	public String getDescription() {

		return "Takes in XLS document as input and gives XML as output to next pipeline step.";
	}

	/*
	 * @Override public IParameters getParameters() { return params; }
	 */

	/*
	 * @Override public void setParameters(IParameters params) { this.params =
	 * (Parameters) params; }
	 */

	@Override
	protected Event handleRawDocument(Event event) {
		 RawDocument rawDoc = (RawDocument)event.getResource();
         BufferedReader reader = null;
         URI inputURI = rawDoc.getInputURI();
         File outFile;
         //FileOutputStream output = null;
			if ( isLastOutputStep() ) {
				outFile = rawDoc.createOutputFile(outputURI);
			}
			else {
				try {
					outFile = File.createTempFile("okp-bom_", ".tmp");
				}
				catch ( Throwable e ) {
					throw new OkapiIOException("Cannot create temporary output.", e);
				}
				outFile.deleteOnExit();
			}
			
			
         String source = inputURI.toString();
         //source.replaceAll("/", "\\\\");
         String dest = source.substring(5,source.lastIndexOf('.'))+".xml";
       
        // String dest="C:\\Users\\ritigupt\\Desktop\\download_center_strings.xml";
         String pattern = "\\<.*?>";
 		Pattern p = Pattern.compile(pattern);
 		Pattern amp=Pattern.compile("&");
 		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));
			Matcher m=null, mAmp=null;
	 		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
	 		writer.write("<asf version =\"1.0\" xmlns=\"http://ns.adobe.com/asf\" locale=\"en\"> \n");
	 		//InputStream input = new BufferedInputStream( new FileInputStream(source));
	 		InputStream input = rawDoc.getStream();
	 		POIFSFileSystem fs = new POIFSFileSystem( input ); 
	 		HSSFWorkbook wb = new HSSFWorkbook(fs); 
	 		HSSFSheet sheet = wb.getSheetAt(0);
	 		String strId=null;
	 		String core=null;
	 		for(int rowNo=1; rowNo<sheet.getLastRowNum(); rowNo++)
	 		{
	 			
	 			HSSFRow row = sheet.getRow(rowNo);
	 			if(row!=null)
	 			{
	 				if(row.getCell(0)!=null && row.getCell(1)!=null)
	 				{
	 					
	 					strId = row.getCell(0).getStringCellValue();
	 					
	 					writer.write("<str name=\""+strId+"\"> \n");
	 					core = row.getCell(1).getStringCellValue();
	 					
	 					StringBuffer sb=new StringBuffer();
	 					int stIndex=0, endIndex=0;
	 					mAmp=amp.matcher(core);
	 					while(mAmp.find())
	 					{
	 						mAmp.appendReplacement(sb, "&amp;");
	 					}
	 					mAmp.appendTail(sb);
	 					m=p.matcher(sb);
	 					core=new String(sb);
	 					while (m.find()) {
	 					    stIndex=m.start();
	 					    endIndex=m.end()-1;
	 					    core=core.substring(0, stIndex)+"&lt;"+core.substring(stIndex+1, endIndex)+"&gt;"+core.substring(endIndex+1);
	 					    sb=new StringBuffer(core);
	 					    m=p.matcher(core);
	 					 }
	 					
	 					writer.write("\t <val>"+core+"</val> \n");
	 					
	 					writer.write("</str> \n");
	 				}
	 					
	 			}
	 			
	 			
	 		}
	 		
	 		writer.write("</asf>");
	 		writer.close();
	 		
	 		/*input.close();
	 		RawDocument newDoc = new RawDocument((new File(dest)).toURI(),"UTF-8",rawDoc.getSourceLocale(),rawDoc.getTargetLocale());
	 		event.setResource(newDoc);*/
	 		input.close(); input = null;
			
			rawDoc.finalizeOutput();
			File copiedFile = new File(this.outputURI);
			FileUtils.copyFile(outFile,copiedFile);
			// Creates the new RawDocument
			event.setResource(new RawDocument(outFile.toURI(), rawDoc.getEncoding(), 
				rawDoc.getSourceLocale(), rawDoc.getTargetLocale()));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}
 		
		return event;
 		
 		
 		
	}
}
