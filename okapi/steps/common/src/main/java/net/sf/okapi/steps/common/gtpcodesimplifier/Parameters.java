/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.common.gtpcodesimplifier;

import net.sf.okapi.common.BaseParameters;
import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

@EditorFor(Parameters.class)
public class Parameters extends BaseParameters implements IEditorDescriptionProvider {

	private static final String FILE_TO_PROCESS = "ftname";
	
	private String ftname;
	public Parameters() {
		reset();
	}
	
	public String getFtname()
	{
		return ftname;
	}
	
	public void setFtname(String ftname)
	{
		this.ftname=ftname;
	}
	
	@Override
	public void reset() {
		ftname="";
	}

	@Override
	public void fromString(String data) {
		reset();
		buffer.fromString(data);
		ftname = buffer.getString(FILE_TO_PROCESS, ftname);
	}

	@Override
	public String toString() {
		buffer.reset();
		buffer.setString(FILE_TO_PROCESS, ftname);
		return buffer.toString();
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(FILE_TO_PROCESS, 
				"Extention of files to be processed by this step", 
				null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("GTP Inline Codes Simplifier", true, false);		
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(FILE_TO_PROCESS));
		tip.setAllowEmpty(true);
		return desc;
	}
}
