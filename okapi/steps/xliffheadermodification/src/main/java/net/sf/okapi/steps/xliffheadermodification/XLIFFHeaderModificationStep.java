package net.sf.okapi.steps.xliffheadermodification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map.Entry;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.skeleton.GenericSkeleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctc.wstx.api.WstxInputProperties;
import com.ctc.wstx.api.WstxOutputProperties;
import com.ctc.wstx.stax.WstxEventFactory;
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;

@UsingParameters(Parameters.class)
public class XLIFFHeaderModificationStep extends BasePipelineStep {
	
	private static final String XLIFF_TAG_ATTR_HREF = "href";
	private static final String XLIFF_TAG_EXTERNAL_FILE = "external-file";
	private static final String XLIFF_TAG_REFERENCE = "reference";
	private static final String XLIFF_TAG_BODY = "body";
	private static final String XLIFF_TAG_HEADER = "header";
	private static final String XLIFF_TAG_FILE = "file";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private Parameters params;
	
	//variables to store the internal state of the step as it processes the pipeline events
	private boolean processingXLIFFDoc = false;
	private boolean startedProcessingSubDoc = false;
	private boolean startedProcessingSubDocBody = false;
	private boolean bodyTagIsEmpty = true;
	private String currentSubDocName = "";
	private String inputRootDir;

	
	public XLIFFHeaderModificationStep () {
		params = new Parameters();
	}
		
	public String getDescription () {
		return "Add/Modify the XLIFF Header Element. Will only process documents of XLIFF mime type. " +
				"Will only process the first sub document in the XLIFF file."
			+ " Expects: Filter Events. Sends back: Filter Events.";
	}

	public String getName () {
		return "XLIFF Header Modification";
	}
	
	@Override
	public IParameters getParameters () {
		return params;
	}
	
	@Override
	public void setParameters (IParameters params) {
		this.params = (Parameters)params;
	}

	@StepParameterMapping(parameterType = StepParameterType.INPUT_ROOT_DIRECTORY)
	public void setInputRootDirectory (String inputRootDir) {
		this.inputRootDir = inputRootDir;
	}
	
	public String getInputRootDirectory() {
		return inputRootDir;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleStartDocument(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleStartDocument(Event event) {
		Event e = super.handleStartDocument(event);
		StartDocument sd = (StartDocument)e.getResource();
		processingXLIFFDoc = sd.getMimeType().equals(MimeTypeMapper.XLIFF_MIME_TYPE);
		bodyTagIsEmpty =true;
		return e;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleEndDocument(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleEndDocument(Event event) {
		Event e = super.handleEndDocument(event);
		processingXLIFFDoc = false;
		bodyTagIsEmpty = true;
		return e;
	}

	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleStartSubDocument(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleStartSubDocument(Event event) {
		Event e = super.handleStartSubDocument(event);
		currentSubDocName = ((StartSubDocument)e.getResource()).getName();
		startedProcessingSubDoc = true;
		startedProcessingSubDocBody = false;
		bodyTagIsEmpty = true;
		return e;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleEndSubDocument(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleEndSubDocument(Event event) {
		Event e = super.handleEndSubDocument(event);
		
		if(bodyTagIsEmpty)
		{
			Ending ed = (Ending)e.getResource();
			addOrUpdateHeader(ed);
		}
		startedProcessingSubDoc = false;
		startedProcessingSubDocBody = false;
		return e;
	}

	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleStartGroup(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleStartGroup(Event event) {
		Event e = super.handleStartGroup(event);
		startedProcessingSubDocBody = true;
		bodyTagIsEmpty = false;
		return e;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleEndGroup(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleEndGroup(Event event) {
		Event e = super.handleEndGroup(event);
		startedProcessingSubDocBody = false;
		return e;
	}
	
	
	/* (non-Javadoc)
	 * @see net.sf.okapi.common.pipeline.BasePipelineStep#handleDocumentPart(net.sf.okapi.common.Event)
	 */
	@Override
	protected Event handleDocumentPart(Event event) {
		Event e = super.handleDocumentPart(event);
		if(processingXLIFFDoc && startedProcessingSubDoc && !startedProcessingSubDocBody) {
			DocumentPart dp = (DocumentPart)e.getResource();
			addOrUpdateHeader(dp);
			bodyTagIsEmpty = false;
		}
		return e;
	}

	/**
	 * @param dp
	 * @throws FactoryConfigurationError
	 */
	private void addOrUpdateHeader(DocumentPart dp)
			throws FactoryConfigurationError {
		try {
			XMLEventFactory eventFact = new WstxEventFactory();
			
			XMLInputFactory inFact = new WstxInputFactory();
			inFact.setProperty(XMLInputFactory.IS_COALESCING, true);
			inFact.setProperty(WstxInputProperties.P_INPUT_PARSING_MODE, WstxInputProperties.PARSING_MODE_FRAGMENT);
			XMLEventReader reader = inFact.createXMLEventReader(new StringReader(dp.getSkeleton().toString()));
			
			XMLOutputFactory outFact = new WstxOutputFactory();
			outFact.setProperty(WstxOutputProperties.P_OUTPUT_VALIDATE_STRUCTURE, "false");
			StringWriter sw = new StringWriter();
			XMLEventWriter writer = outFact.createXMLEventWriter(sw);
			
			XMLEvent e = copyUptoElement(reader, writer, XLIFF_TAG_HEADER, XLIFF_TAG_BODY, false);
			if(e==null || (e.isStartElement() && e.asStartElement().getName().getLocalPart().equals(XLIFF_TAG_BODY))) {
				addHeaderAndChildren(writer, eventFact);	
			} else {
				writer.add(e);
				e = copyUptoElement(reader, writer, XLIFF_TAG_REFERENCE, XLIFF_TAG_HEADER, true);
				if(e==null || (e.isEndElement() && e.asEndElement().getName().getLocalPart().equals(XLIFF_TAG_HEADER))) {
					addRefernceAndChildren(writer, eventFact);
				} else {
					writer.add(e);
					addExternalFile(writer, eventFact);
					e = null;
				}
			}
			if(e!=null) {
				writer.add(e);
			}
			copyAllRemaining(reader, writer);

			//add a dummy whitespace characters event to close out the body start tag which wstx sometimes leaves open
			writer.add(eventFact.createCharacters("\n"));
			writer.flush();
			
			GenericSkeleton skeleton = new GenericSkeleton();			
			skeleton.append(sw.toString());
			dp.setSkeleton(skeleton);
		} catch (XMLStreamException e1) {
			throw new OkapiIOException(e1.getMessage(), e1);
		}
	}
	
	/**
	 * @param dp
	 * @throws FactoryConfigurationError
	 */
	private void addOrUpdateHeader(Ending ed)
			throws FactoryConfigurationError {
		try {
			XMLEventFactory eventFact = new WstxEventFactory();
			
			XMLInputFactory inFact = new WstxInputFactory();
			inFact.setProperty(XMLInputFactory.IS_COALESCING, true);
			inFact.setProperty(WstxInputProperties.P_INPUT_PARSING_MODE, WstxInputProperties.PARSING_MODE_FRAGMENT);
			XMLEventReader reader = inFact.createXMLEventReader(new StringReader(ed.getSkeleton().toString()));
			
			XMLOutputFactory outFact = new WstxOutputFactory();
			outFact.setProperty(WstxOutputProperties.P_OUTPUT_VALIDATE_STRUCTURE, "false");
			StringWriter sw = new StringWriter();
			XMLEventWriter writer = outFact.createXMLEventWriter(sw);
			
			XMLEvent e = copyUptoElement(reader, writer, XLIFF_TAG_HEADER, XLIFF_TAG_BODY, false);
			if(e==null || (e.isStartElement() && e.asStartElement().getName().getLocalPart().equals(XLIFF_TAG_BODY))) {
				addHeaderAndChildren(writer, eventFact);
				
			} else {
				writer.add(e);
				e = copyUptoElement(reader, writer, XLIFF_TAG_REFERENCE, XLIFF_TAG_HEADER, true);
				if(e==null || (e.isEndElement() && e.asEndElement().getName().getLocalPart().equals(XLIFF_TAG_HEADER))) {
					addRefernceAndChildren(writer, eventFact);
				} else {
					writer.add(e);
					addExternalFile(writer, eventFact);
					e = null;
				}
			}
			if(e!=null) {
				writer.add(e);
			}
			copyAllRemaining(reader, writer);

			//add end file tag as a single file tag in ebd as discarded as exception while copyAllRemaining is executed.
			writer.add(eventFact.createCharacters("</"+XLIFF_TAG_FILE+">"));
			writer.flush();
			
			GenericSkeleton skeleton = new GenericSkeleton();			
			skeleton.append(sw.toString());
			ed.setSkeleton(skeleton);
		} catch (XMLStreamException e1) {
			throw new OkapiIOException(e1.getMessage(), e1);
		}
	}

	private void copyAllRemaining(XMLEventReader reader, XMLEventWriter writer) throws XMLStreamException {
		copyUptoElement(reader, writer, null, null, false);
	}

	private void addExternalFile(XMLEventWriter writer, XMLEventFactory eventFact) throws XMLStreamException {
		writer.add(eventFact.createStartElement("", "", XLIFF_TAG_EXTERNAL_FILE));
		writer.add(eventFact.createAttribute(XLIFF_TAG_ATTR_HREF, buildExternalFileURL()));
		writer.add(eventFact.createEndElement("", "", XLIFF_TAG_EXTERNAL_FILE));
	}

	/**
	 * @return
	 */
	private String buildExternalFileURL() {
		try {
			if(!params.isReadDocNameFromOriginalFile()) {
				return URLUtils.buildPreviewURL(params.getExternalFileURLPrefix()==null ? "" : params.getExternalFileURLPrefix(),
						currentSubDocName.trim(), params.getExternalFileURLSuffix()==null ? "" : params.getExternalFileURLSuffix().trim());
			} else {
				Entry<String, String> attributeFromOriginalFile = readDocNameFromOriginalFile();
				//don't use suffix from the step params if sourcePreviewURL attribute was present in the input xliff file
				String suffix = URLUtils.ATTR_SOURCE_PREVIEW_URL.equals(attributeFromOriginalFile.getKey()) ? null : params.getExternalFileURLSuffix();
				return URLUtils.buildPreviewURL(params.getExternalFileURLPrefix()==null ? "" : params.getExternalFileURLPrefix(), 
						attributeFromOriginalFile.getValue(), suffix==null ? "" : suffix.trim());				
			}
		} catch (Exception e) {
			// don't throw error here, source preview URL in WS will be wrong
			logger.warn(e.getMessage(), e);
			return "ERROR";
		}
	}

	/**
	 * @return The attribute name (key) and attribute value read from incoming XLIFF files 'file' tag. 
	 * 	Key will be either 'sourcePreviewURL' or 'original'. If file tag has sourcePreviewURL attribute then use that else
	 *  use 'original' attribute. Whichever attribute is used to read the value it's returned as the 'key'
	 * @throws FileNotFoundException
	 * @throws XMLStreamException
	 */
	private Entry<String, String> readDocNameFromOriginalFile() throws FileNotFoundException, XMLStreamException {
		String originalFilePath = inputRootDir;
		if(!inputRootDir.endsWith(File.separator) && !currentSubDocName.startsWith(File.separator)) {
			originalFilePath += File.separator;
		} else if(inputRootDir.endsWith(File.separator) && currentSubDocName.startsWith(File.separator)) {
			originalFilePath = originalFilePath.substring(0, originalFilePath.length()-File.separator.length());
		}
		originalFilePath += currentSubDocName;
		
		return URLUtils.getOriginalAttributeFromXLIFFFile(originalFilePath);
	}

	private void addRefernceAndChildren(XMLEventWriter writer, XMLEventFactory eventFact) throws XMLStreamException {
		writer.add(eventFact.createStartElement("", "", XLIFF_TAG_REFERENCE));
		addExternalFile(writer,eventFact);
		writer.add(eventFact.createEndElement("", "", XLIFF_TAG_REFERENCE));
	}

	private void addHeaderAndChildren(XMLEventWriter writer, XMLEventFactory eventFact) throws XMLStreamException {
		writer.add(eventFact.createStartElement("", "", XLIFF_TAG_HEADER));
		addRefernceAndChildren(writer,eventFact);
		writer.add(eventFact.createEndElement("", "", XLIFF_TAG_HEADER));
	}

	/**
	 * Copies events from reader to writer except the start document event.
	 * If stop2IsClosingTag is false then stops as soon as start element event for either 
	 * stopElement1 or stopElement2 is reached.
	 * If stop2IsClosingTag is true then stops as soon as either start element event for 
	 * stopElement1 or end element event for stopElement2 is reached. 
	 * If either stopElement1 or stopElement2 is null then it's not used as a stop criteria.
	 * If both stopElement1 or stopElement2 are null then all events are copied untill end of input.
	 * @param reader
	 * @param writer
	 * @param stopElement1
	 * @param stopElement2
	 * @param stop2IsClosingTag
	 * @return
	 * @throws XMLStreamException
	 */
	private XMLEvent copyUptoElement(XMLEventReader reader,
			XMLEventWriter writer, String stopElement1, String stopElement2, boolean stop2IsClosingTag) throws XMLStreamException {
		while(reader.hasNext()) {
			XMLEvent e;
			try {
				e = reader.nextEvent();
			} catch (XMLStreamException e1) {
				//do not report this exception, since we're parsing an xml fragment it's expected
				//that the last few tags may not be closed properly
				return null;
			}
			switch(e.getEventType()) {
			case XMLEvent.START_DOCUMENT:
				break;
			case XMLEvent.START_ELEMENT:
				StartElement se = e.asStartElement();
				QName elementName = se.getName();
				if((stopElement1!= null && elementName.getLocalPart().equals(stopElement1)) || 
						(!stop2IsClosingTag && stopElement2!=null && elementName.getLocalPart().equals(stopElement2)) ) {
					return e;
				}
				writer.add(e);
				break;
			case XMLEvent.END_ELEMENT:
				EndElement ee = e.asEndElement();
				QName closingElementName = ee.getName();
				if(stop2IsClosingTag && stopElement2!=null && closingElementName.getLocalPart().equals(stopElement2)) {
					return e; 
				}
				writer.add(e);
				break;
			default:
				writer.add(e);
				break;
			}
		}
		return null;
	}

}
