/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffheadermodification;

import net.sf.okapi.common.BaseParameters;
import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(Parameters.class)
public class Parameters extends BaseParameters implements IEditorDescriptionProvider {	
	
	private static final String PARAM_NAME_EXTERNAL_FILE_URL_PREFIX = "externalFileURLPrefix";
	private static final String PARAM_NAME_EXTERNAL_FILE_URL_SUFFIX = "externalFileURLSuffix";
	private static final String PARAM_NAME_READ_DOCNAME_FROM_ORIGINAL_FILE = "readDocNameFromOriginalFile";
	
	private String externalFileURLPrefix = null;
	private String externalFileURLSuffix = null;
	private boolean readDocNameFromOriginalFile = false;
	
	public Parameters () {
		reset();
	}
	
	public void reset() {
		externalFileURLPrefix = null;
		externalFileURLSuffix = null;
		readDocNameFromOriginalFile = false;
	}

	public void fromString (String data) {
		reset();
		buffer.fromString(data);
		externalFileURLPrefix = buffer.getString(PARAM_NAME_EXTERNAL_FILE_URL_PREFIX, null);
		externalFileURLSuffix = buffer.getString(PARAM_NAME_EXTERNAL_FILE_URL_SUFFIX, null);
		readDocNameFromOriginalFile = buffer.getBoolean(PARAM_NAME_READ_DOCNAME_FROM_ORIGINAL_FILE, false);
	}

	@Override
	public String toString() {
		buffer.reset();
		buffer.setString(PARAM_NAME_EXTERNAL_FILE_URL_PREFIX, externalFileURLPrefix);
		buffer.setString(PARAM_NAME_EXTERNAL_FILE_URL_SUFFIX, externalFileURLSuffix);
		buffer.setBoolean(PARAM_NAME_READ_DOCNAME_FROM_ORIGINAL_FILE, readDocNameFromOriginalFile);
		return buffer.toString();
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(PARAM_NAME_EXTERNAL_FILE_URL_PREFIX, "External File URL Prefix", "URL Prefix to be concatenated before the " +
				"document name, to set in the input XLIFF file header as the value of the external-file href attribute. ");
		desc.add(PARAM_NAME_EXTERNAL_FILE_URL_SUFFIX, "External File URL Suffix", "URL Suffix to be concatenated after the " +
				"document name, to set in the input XLIFF file header as the value of the external-file href attribute. ");
		desc.add(PARAM_NAME_READ_DOCNAME_FROM_ORIGINAL_FILE, "Read Doc Name From Original File", "If true then read the document name " +
				"from the original source file. Otherwise read the document name from the DOCUMENT_START filter event. ");
		return desc;
	}
	
	@Override
	public EditorDescription createEditorDescription(
			ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("XLIFFHeaderModification Step Parameters");		
		desc.addTextInputPart(paramDesc.get(PARAM_NAME_EXTERNAL_FILE_URL_PREFIX));
		desc.addTextInputPart(paramDesc.get(PARAM_NAME_EXTERNAL_FILE_URL_SUFFIX));
		desc.addCheckboxPart(paramDesc.get(PARAM_NAME_READ_DOCNAME_FROM_ORIGINAL_FILE));
		return desc;		
	}

	/**
	 * @return the externalFileURLPrefix
	 */
	public String getExternalFileURLPrefix() {
		return externalFileURLPrefix;
	}

	/**
	 * @param externalFileURLPrefix the externalFileURLPrefix to set
	 */
	public void setExternalFileURLPrefix(String externalFileURLPrefix) {
		this.externalFileURLPrefix = externalFileURLPrefix;
	}

	/**
	 * @return the externalFileURLSuffix
	 */
	public String getExternalFileURLSuffix() {
		return externalFileURLSuffix;
	}

	/**
	 * @param externalFileURLSuffix the externalFileURLSuffix to set
	 */
	public void setExternalFileURLSuffix(String externalFileURLSuffix) {
		this.externalFileURLSuffix = externalFileURLSuffix;
	}

	public boolean isReadDocNameFromOriginalFile() {
		return readDocNameFromOriginalFile;
	}

	public void setReadDocNameFromOriginalFile(boolean readDocNameFromOriginalFile) {
		this.readDocNameFromOriginalFile = readDocNameFromOriginalFile;
	}
}
