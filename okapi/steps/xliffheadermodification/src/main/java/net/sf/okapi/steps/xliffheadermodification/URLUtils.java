package net.sf.okapi.steps.xliffheadermodification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.HashMap;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class URLUtils {
	
	private static final String ATTR_ORIGINAL = "original";
	public static final String ATTR_SOURCE_PREVIEW_URL = "sourcePreviewURL";

	public static String buildPreviewURL(String prefix, String fileName, String suffix) {
		StringBuilder externalFileURL = new StringBuilder(prefix);

		if(endsWithSlash(externalFileURL) && beginsWithSlash(fileName)) {
			//remove the extra slash
			externalFileURL.setLength(externalFileURL.length()-1);
		} else if (!endsWithSlash(externalFileURL) && !beginsWithSlash(fileName)) {
			externalFileURL.append('/');
		}
		externalFileURL.append(fileName);
		externalFileURL.append(suffix);
		
		return externalFileURL.toString();
	}
	
	/**
	 * @param originalFilePath
	 * @return returns the value of attribute 'sourcePreviewURL' from 'file' tag. 
	 *   If that attribute does not exist return the value of 'original' attribute. If both attributes do not exist throw error.
	 * @throws FactoryConfigurationError
	 * @throws XMLStreamException
	 * @throws FileNotFoundException
	 */
	public static Entry<String, String> getOriginalAttributeFromXLIFFFile(String originalFilePath)
			throws FactoryConfigurationError, XMLStreamException,
			FileNotFoundException {
		Map<String, String> attributes = getAttributesFromXLIFFFile(originalFilePath, "file");
		if(attributes.containsKey(ATTR_SOURCE_PREVIEW_URL)) {
			return new SimpleImmutableEntry<String, String>(ATTR_SOURCE_PREVIEW_URL, attributes.get(ATTR_SOURCE_PREVIEW_URL));
		}
		if(attributes.containsKey(ATTR_ORIGINAL)) {
			return new SimpleImmutableEntry<String, String>(ATTR_ORIGINAL, attributes.get(ATTR_ORIGINAL));
		}
		throw new IllegalArgumentException("Could not find a sourcePreviewURL or original attribute in the file tag, in file:"+originalFilePath);
	}

	public static Map<String, String> getAttributesFromXLIFFFile(String originalFilePath, String tagName)
			throws FactoryConfigurationError, XMLStreamException,
			FileNotFoundException {
		XMLInputFactory f = XMLInputFactory.newInstance();
		XMLStreamReader r = null;
		try {
			r = f.createXMLStreamReader( new BufferedReader( new FileReader(originalFilePath)) );

			while(r.hasNext()) {
			    r.next();
			    switch(r.getEventType()) {
			    case XMLStreamConstants.START_ELEMENT:
			    	if(r.getLocalName().equals(tagName)) {
			    		if(r.getNamespaceURI()==null || r.getNamespaceURI().equals("urn:oasis:names:tc:xliff:document:1.2")) {
			    			int attrCount = r.getAttributeCount();
			    			HashMap<String, String> attributes = new HashMap<String, String>(attrCount);
			    			for(int i=0; i<attrCount; i++) {
			    				attributes.put(r.getAttributeLocalName(i), r.getAttributeValue(i));
			    			}
			    			return attributes;
			    		}
			    	}
			    	break;
			    default:
			    	break;
			    }
			}
			throw new IllegalArgumentException("Could not find a "+tagName+" tag, in file:"+originalFilePath);
		} finally {
			if(r!=null) {
				try {
					r.close();
				} catch (XMLStreamException e) {
					//ignore
				}
			}
		}
	}

	public static String getAttributeFromXLIFFFile(String outputFilePath, String element, String attribute) throws FileNotFoundException, FactoryConfigurationError, XMLStreamException {
		return getAttributesFromXLIFFFile(outputFilePath, element).get(attribute);
	}

	private static boolean endsWithSlash(StringBuilder str) {
		return str.length()>0 && str.charAt(str.length()-1)=='/';
	}

	private static boolean beginsWithSlash(CharSequence str) {
		return str.length()>0 && str.charAt(0)=='/';
	}

}
