package net.sf.okapi.steps.xliffheadermodification;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XFilters;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.steps.EventLogger;
import net.sf.okapi.lib.extra.steps.TextUnitLogger;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.rainbowkit.creation.ExtractionStep;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestXLIFFHeaderModificationStep {

	private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
	
	@Test
	public void testClassInstantiation() throws ClassNotFoundException {
		Class<?> cls = Class.forName(ClassUtil.getQualifiedClassName(XLIFFHeaderModificationStep.class));
		assertNotNull(cls);
	}
	
	@Test
	public void testClassInstantiation_from_target_class_file() throws ClassNotFoundException, MalformedURLException {
		File file = new File(ClassUtil.getTargetPath(XLIFFHeaderModificationStep.class) + 
				ClassUtil.getClassName(XLIFFHeaderModificationStep.class) + ".class");
		
		// Create a temporary class loader (mimics Rainbow plug-in manager)
		URL[] tmpUrls = new URL[1]; 
		URL url = file.toURI().toURL();
		tmpUrls[0] = url;
		URLClassLoader loader = URLClassLoader.newInstance(tmpUrls);
		
		Class<?> cls = Class.forName(ClassUtil.getQualifiedClassName(XLIFFHeaderModificationStep.class), false, loader);
		assertNotNull(cls);
	}
	
	@Test
	public void testNoInputHeader() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_noheader.xlf").toURI();
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".html");
		
		transformAndCheck(inputFIleURI, outputFileURI, goldFileURI, params);
	}
	
	@Test
	public void testWithInputHeader() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_withheader.xlf").toURI();
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".html");
		
		transformAndCheck(inputFIleURI, outputFileURI, goldFileURI, params);
	}

	@Test
	public void testNoGroupTag() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_nogrouptag.xlf").toURI();
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".html");
		
		transformAndCheck(inputFIleURI, outputFileURI, goldFileURI, params);
	}
	
	@Test
	public void testMultiFileTag() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_multiFile.docx.xlf").toURI();
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".html");
		
		transformAndCheck(inputFIleURI, outputFileURI, goldFileURI, params);
	}
	
	@Test
	public void testReadNameFromOriginalFile() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_readNameFromFile.xlf").toURI();
		URI inputRootURI = inputFIleURI.resolve("..");
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".html");
		params.setReadDocNameFromOriginalFile(true);
		
		executeStepInPipeline(inputFIleURI, inputRootURI, outputFileURI, params);
		
		URL generatedOutputFileURL = inputRootURI.resolve("pack1/work/test_xliff/test_readNameFromFile.xlf.xlf").toURL();
		Diff myDiff = new Diff( IOUtils.toString(generatedOutputFileURL.openStream()), IOUtils.toString(goldFileURI.toURL().openStream()) );
		assertTrue("Generated " + generatedOutputFileURL+" files should be similar to gold file", myDiff.similar());
	}

	/**
	 * Test the sourcePreviewURL attribute from input XLF is used
	 * @throws URISyntaxException
	 * @throws SAXException
	 * @throws IOException
	 */
	@Test
	public void testReadNameFromOriginalFile_previewURLAttribute() throws URISyntaxException, SAXException, IOException {
		URI inputFIleURI = getClass().getResource("/test_xliff/test_readSourcePreviewURLAttribute.xlf").toURI();
		URI inputRootURI = inputFIleURI.resolve("..");
		URI outputFileURI = new URI(inputFIleURI.toString()+".out");
		URI goldFileURI = new URI(inputFIleURI.toString()+".gold");
		Parameters params = new Parameters();
		params.setExternalFileURLPrefix("http://test.somewhere.com/");
		params.setExternalFileURLSuffix(".xyz");
		params.setReadDocNameFromOriginalFile(true);
		
		executeStepInPipeline(inputFIleURI, inputRootURI, outputFileURI, params);
		
		URL generatedOutputFileURL = inputRootURI.resolve("pack1/work/test_xliff/test_readSourcePreviewURLAttribute.xlf.xlf").toURL();
		Diff myDiff = new Diff( IOUtils.toString(generatedOutputFileURL.openStream()), IOUtils.toString(goldFileURI.toURL().openStream()) );
		assertTrue("Generated " + generatedOutputFileURL+" should be similar to gold file", myDiff.similar());
	}

	private void executeStepInPipeline(URI inputFIleURI, URI inputRootURI, URI outputFileURI, Parameters params) {
		XLIFFFilter xliffFilter = new XLIFFFilter();
		XmlStreamFilter xmllFilter = new XmlStreamFilter();
		//((net.sf.okapi.filters.xliff.Parameters)xliffFilter.getParameters()).setOntramUseCase(false);
		
		ExtractionStep xtractionStep = new ExtractionStep();
		net.sf.okapi.steps.rainbowkit.creation.Parameters xtractParams = new net.sf.okapi.steps.rainbowkit.creation.Parameters();
		xtractParams.setSendOutput(true);
		xtractParams.setSendOutputFilterConfig("okf_xliff");
		xtractionStep.setParameters(xtractParams);
		xtractionStep.setOutputURI(inputRootURI);
		
		XLIFFHeaderModificationStep testStep = new XLIFFHeaderModificationStep();
		testStep.setParameters(params);

		FilterEventsToRawDocumentStep outStep = new FilterEventsToRawDocumentStep();
		outStep.setOutputURI(outputFileURI);
		
		new XPipeline(
				"Lists events after TextPool Translation Import step",
				new XFilters(xliffFilter, xmllFilter),
				new File(inputRootURI).getAbsolutePath()+File.separator,
				new XBatch(
						new XBatchItem(
								inputFIleURI,
								"UTF-8",
								new LocaleId("en-us"),
								new LocaleId("cs-cz"))
						),						
				new RawDocumentToFilterEventsStep(xmllFilter),
			 	xtractionStep,
				new RawDocumentToFilterEventsStep(xliffFilter),
				testStep,
/*	
			 	new EventLogger(), // Displays info at the FINE logger level
			 	new TextUnitLogger(), // Displays info at the FINE logger level
*/
				outStep
		).execute();
	}
	
	/**
	 * @param inputFIleURI
	 * @param outputFileURI
	 * @param goldFileURI
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws MalformedURLException 
	 */
	private void transformAndCheck(URI inputFIleURI, URI outputFileURI,
			URI goldFileURI, Parameters params) throws MalformedURLException, SAXException, IOException {
		XLIFFFilter xliffFilter = new XLIFFFilter();
		//((net.sf.okapi.filters.xliff.Parameters)xliffFilter.getParameters()).setOntramUseCase(false);
		
		XLIFFHeaderModificationStep testStep = new XLIFFHeaderModificationStep();
		testStep.setParameters(params);

		FilterEventsToRawDocumentStep outStep = new FilterEventsToRawDocumentStep();
		outStep.setOutputURI(outputFileURI);
		
		new XPipeline(
				"Lists events after TextPool Translation Import step",
				new XBatch(
						new XBatchItem(
								inputFIleURI,
								"UTF-8",
								new LocaleId("en-us"),
								new LocaleId("cs-cz"))
						),						
				new RawDocumentToFilterEventsStep(xliffFilter),
				testStep,
/*	
			 	new EventLogger(), // Displays info at the FINE logger level
			 	new TextUnitLogger(), // Displays info at the FINE logger level
*/
				outStep
		).execute();
		
		Diff myDiff = new Diff( IOUtils.toString(outputFileURI.toURL().openStream()),
				IOUtils.toString(goldFileURI.toURL().openStream()) );
		assertTrue("xml files should be similar", myDiff.similar());
	}	
}
