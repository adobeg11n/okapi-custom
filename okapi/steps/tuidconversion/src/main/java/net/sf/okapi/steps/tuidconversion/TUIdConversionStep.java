package net.sf.okapi.steps.tuidconversion;

import java.util.Stack;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextUnit;

@UsingParameters()
public class TUIdConversionStep extends BasePipelineStep {
	
	private Stack<String> groupIds = new Stack<String>();
	
	public TUIdConversionStep () {
	}
		
	public String getDescription () {
		return "Convert the TU Ids by appending the group ids that the TU is a child of."
			+ " Expects: Filter Events. Sends back: Filter Events.";
	}

	public String getName () {
		return "TU Id Conversion";
	}

	@Override
	protected Event handleStartGroup(Event event) {
		Event e = super.handleStartGroup(event);
		StartGroup sg = (StartGroup)e.getResource();
		groupIds.push(sg.getId());
		return e;
	}

	@Override
	protected Event handleEndGroup(Event event) {
		Event e = super.handleStartGroup(event);
		if(!groupIds.isEmpty())
			groupIds.pop();
		return e;
	}

	@Override
	protected Event handleTextUnit(Event event) {
		Event e = super.handleStartGroup(event);
		TextUnit tu = (TextUnit)e.getResource();
		if (!groupIds.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (String id : groupIds){
				sb.append(id).append("/");
			}
			tu.setId(sb.toString()+tu.getId());
		}
		return e;
	}
}
