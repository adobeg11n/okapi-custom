/*******************************************************************************
 * Copyright (C) 2009-2011 by Andrae AG all rights reserved
 *******************************************************************************/
package net.sf.okapi.steps.tuidconversion;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.steps.EventLogger;
import net.sf.okapi.lib.extra.steps.TextUnitLogger;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

import org.junit.Test;

public class TestTUIdConversionStep {

	private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
	
	@Test
	public void testClassInstantiation() throws ClassNotFoundException {
		Class<?> cls = Class.forName(ClassUtil.getQualifiedClassName(TUIdConversionStep.class));
		assertNotNull(cls);
	}
	
	@Test
	public void testClassInstantiation_from_target_class_file() throws ClassNotFoundException, MalformedURLException {
		File file = new File(ClassUtil.getTargetPath(TUIdConversionStep.class) + 
				ClassUtil.getClassName(TUIdConversionStep.class) + ".class");
		
		// Create a temporary class loader (mimics Rainbow plug-in manager)
		URL[] tmpUrls = new URL[1]; 
		URL url = file.toURI().toURL();
		tmpUrls[0] = url;
		URLClassLoader loader = URLClassLoader.newInstance(tmpUrls);
		
		Class<?> cls = Class.forName(ClassUtil.getQualifiedClassName(TUIdConversionStep.class), false, loader);
		assertNotNull(cls);
	}
	
	@Test
	public void testOriginal() throws URISyntaxException {
		LOGGER.fine("=== testOriginal");
		XLIFFFilter xliffFilter = new XLIFFFilter();
		//((net.sf.okapi.filters.xliff.Parameters)xliffFilter.getParameters()).setOntramUseCase(false);
		new XPipeline(
				"Lists events of XLIFF Filter",
				new XBatch(
						new XBatchItem(
								getClass().getResource("/xliff_test_translated/test.xlf").toURI(),
								"UTF-8",
								new LocaleId("en-us"),
								new LocaleId("cs-cz"))
						),						
				new RawDocumentToFilterEventsStep(xliffFilter),
				new EventLogger(), // Displays info at a FINE logger level
				new TextUnitLogger() // Displays info at a FINE logger level
		).execute();
	}
	
	@Test
	public void testTrlImport_listing() throws URISyntaxException {
		LOGGER.fine("=== testTrlImport_listing");
		XLIFFFilter xliffFilter = new XLIFFFilter();
		//((net.sf.okapi.filters.xliff.Parameters)xliffFilter.getParameters()).setOntramUseCase(false);
		new XPipeline(
				"Lists events after TextPool Translation Import step",
				new XBatch(
						new XBatchItem(
								getClass().getResource("/xliff_test_translated/test.xlf").toURI(),
								"UTF-8",
								new LocaleId("en-us"),
								new LocaleId("cs-cz"))
						),						
				new RawDocumentToFilterEventsStep(xliffFilter),
				new TUIdConversionStep(),
				new EventLogger(), // Displays info at the FINE logger level
				new TextUnitLogger() // Displays info at the FINE logger level
		).execute();
	}
	
	
}
